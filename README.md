# GUI Running Guide
gui.py allows user to select pretrained model from files and test the results with images.


#### Pre-requisite libraries
- keras
- pandas
- tkinter


#### Execute commands
```
    $ python3 gui.py "YOUR_WORKING_DIRECTORY"
```


#### Notice
1. In order to check the label of image and compare the performance, only images under the "test" folder are usable.
(Other images are capable for prediction but error message will be thrown out during comparison.)
2. "test" folder and "train_labels.csv" must be included and under the same directory, which has been specified in arguments on execution.


#### Demo
![Screenshot 1](1.png)
![Screenshot 2](2.png)
![Screenshot 3](3.png)
![Screenshot 4](4.png)

# Code

Main training code located in `train.py`.

Change `/data` directory to run.