#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 16:40:24 2019

@author: lujia_yang
"""
import pandas as pd
from tkinter import *
import tkinter.filedialog
from PIL import ImageTk, Image
import numpy as np
import keras.models 
from glob import glob 
from keras_preprocessing.image import img_to_array,load_img
import os
import argparse


parser = argparse.ArgumentParser(description='Optional app description')
parser.add_argument('path', nargs='?', type=str, default="/Users/JasmineY/Desktop/AI/project/",
                    help='Input your working directory')
args = parser.parse_args()

path= args.path
test_path = path+'test/'
model=False

def md():
    global model
    mdir = tkinter.filedialog.askopenfilename(
        parent=root, initialdir=path,
        title='Choose file',filetypes=[('models', '.h5')]
        )
    model=keras.models.load_model(mdir)
    #model=keras.models.load_model("./mobilenet.h5")
    #_epoch-01_loss-0.2703_val_loss-0.2696
    #model=keras.models.load_model("./mobilenet2.h5")
    #_epoch-06_loss-0.1349_val_loss-0.2766
    
def load():
    f = tkinter.filedialog.askopenfilename(
        parent=root, initialdir=test_path,
        title='Choose file',filetypes=[('png images', '.png'),
                                       ('tif images', '.tif'),
                                       ('jpg images', '.jpg')]
        )
    img = ImageTk.PhotoImage(Image.open(f))
    canvas.create_image(canvas.winfo_width()/2,canvas.winfo_height()/2,anchor=CENTER,image=img)
    canvas.img=img
    
    train(f)
    #T.insert(END, "Just an EXAMPLE\nfor the predicted label of IMG\n")
 
  
def train(f):
    if model==False:
        example.set("Please choose your model first!")
    else:
        image=load_img(f, target_size=(96, 96))
        image = img_to_array(image)
        image = (image -  127.5) / (127.5)
        image = image.reshape((1,) + image.shape)
        new_predictions = model.predict(image)
        f=f.strip(test_path)[:-4]
        rowData = labels.loc[f]["label"]
        print(new_predictions)
        if new_predictions[0][0]>0.5:
            p=0
        else:
            p=1   
        example.set("Predict: {0}\nLabel: {1}".format(p, rowData))
    
labels = pd.read_csv(path+'train_labels.csv')
labels.set_index('id', inplace=True)
print(labels.head(3))
root = Tk()
root.title('GUI')
b2 = Button(root, text='Load Model', command=md)
b1 = Button(root, text='Load Image', command=load)
canvas = Canvas(root,width=500,height=300)
example = StringVar()
T = Label(root, textvariable=example)
T.pack()
#my_image=ImageTk.PhotoImage(Image.open('/Users/JasmineY/Desktop/AI/WEEK4/imagenet_first2500/imagespart/ILSVRC2012_val_00000012.JPEG'))
#canvas.create_image(0,0,anchor=NW,image=my_image)
b2.pack()
b1.pack()

canvas.pack()
T.pack(side = BOTTOM)

root.mainloop()