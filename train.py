# -*- coding: utf-8 -*-

import os
import tensorflow as tf
from keras.applications.mobilenet import MobileNet
from keras.models import Model
from keras.layers import GlobalAveragePooling2D, Reshape, Dropout, Conv2D, Activation, Flatten
from keras import backend as K
from keras.optimizers import Adam
from keras.utils import HDF5Matrix
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, TerminateOnNaN, CSVLogger
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from keras.applications.resnet50 import ResNet50
from keras.models import Sequential
from keras import layers
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, BatchNormalization, Activation
from keras.layers import Conv2D, MaxPool2D

from data_loader.classification_data_generator import DataGenerator
from data_loader.classification_photometric_ops import Standardize
from data_loader.data_augmentation_chain_satellite import DataAugmentationToolSet

# configure tensorflow backend
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
tf.keras.backend.set_session(tf.Session(config=config))

# hyper params
n_classes = 2
dropout = 1e-3
data_dir = 'data'

# load data
train_x_path = os.path.join(data_dir + os.sep, 'camelyonpatch_level_2_split_train_x.h5')
train_y_path = os.path.join(data_dir + os.sep, 'camelyonpatch_level_2_split_train_y.h5')
val_x_path = os.path.join(data_dir + os.sep, 'camelyonpatch_level_2_split_valid_x.h5')
val_y_path = os.path.join(data_dir + os.sep, 'camelyonpatch_level_2_split_valid_y.h5')

train_x = HDF5Matrix(train_x_path, 'x')
train_y = HDF5Matrix(train_y_path, 'y')

val_x = HDF5Matrix(val_x_path, 'x')
val_y = HDF5Matrix(val_y_path, 'y')

train_dataset = DataGenerator(hdf5_images_path=train_x_path,
                              hdf5_images_dataset='x',
                              hdf5_labels_path=train_y_path,
                              hdf5_labels_dataset='y',
                              n_classes=n_classes)

val_dataset = DataGenerator(hdf5_images_path=val_x_path,
                            hdf5_images_dataset='x',
                            hdf5_labels_path=val_y_path,
                            hdf5_labels_dataset='y',
                            n_classes=n_classes)

augmentation_chain = DataAugmentationToolSet(resize=False,
                                             subtrahend=127.5,
                                             divisor=127.5,
                                             random_brightness=(-64, 64, 0.5),
                                             random_hue=(14, 0.5),
                                             random_flip=0.5,
                                             random_rotate=([90, 180, 270], 0.5))

# standardization
standardize = Standardize()

train_generator = train_dataset.generate(batch_size=32,
                                         shuffle=True,
                                         transformations=[augmentation_chain],
                                         to_one_hot=True,
                                         returns={'processed_images', 'labels'})

val_generator = val_dataset.generate(batch_size=32,
                                     shuffle=False,
                                     transformations=[standardize],
                                     to_one_hot=True,
                                     returns={'processed_images', 'labels'})

# clear models
K.clear_session()

#####################################################
# uncomment for mobileNet
# # load pretrained model
# base_model = MobileNet(input_shape=(96, 96, 3),
#                        alpha=1.0,
#                        depth_multiplier=1,
#                        dropout=dropout,
#                        include_top=False,
#                        weights='imagenet',
#                        pooling=None)
#
# out = base_model.output
#
#
#
# # pooling
# out = GlobalAveragePooling2D()(out)
#
# # reshape
# out = Reshape((1, 1, int(1024 * 1.0)), name='reshape_1')(out)
#
# # dropout
# out = Dropout(dropout, name='dropout')(out)
#
# # conv
# out = Conv2D(n_classes, (1, 1), padding='same', name='conv_preds')(out)
#
# # activation
# out = Activation('softmax', name='activation_softmax')(out)
# out = Reshape((n_classes,), name='reshape_2')(out)
#
# model = Model(inputs=base_model.input, outputs=out)
############################################################


# optimizer
adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

############################################################
# resNet
dropout_fc = 0.5

base_model = ResNet50(
    weights='imagenet',
    include_top=False,
    input_shape=(96, 96, 3),
)

model = Sequential()
model.add(base_model)
model.add(Flatten())
model.add(Dense(256, use_bias=False))
model.add(BatchNormalization())
model.add(Activation("relu"))
model.add(Dropout(dropout_fc))
model.add(Dense(2, activation="sigmoid"))

# freeze layer
base_model.Trainable = True

set_trainable = False
for layer in base_model.layers:
    if layer.name == 'res5a_branch2a':
        set_trainable = True
    if set_trainable:
        layer.trainable = True
    else:
        layer.trainable = False
#############################################################

model.compile(optimizer=adam, loss='categorical_crossentropy', metrics=['accuracy'])

# define check points
model_checkpoint = ModelCheckpoint(filepath='model epoch-{epoch:02d}_loss-{loss:.4f}_val_loss-{val_loss:.4f}.h5',
                                   monitor='val_loss',
                                   verbose=1,
                                   save_best_only=False,
                                   save_weights_only=False,
                                   mode='auto',
                                   period=1)

# log training data
csv_logger = CSVLogger(filename='acc_loss_log.csv',
                       separator=',',
                       append=True)


# TODO decay learning rate
def learning_rate_sch(epoch):
    return 0.001


learning_rate_scheduler = LearningRateScheduler(schedule=learning_rate_sch, verbose=1)

# early stopping
early_stopper = EarlyStopping(monitor='val_loss', patience=2, verbose=1, restore_best_weights=True)
reduce_lr = ReduceLROnPlateau(monitor='val_loss', patience=1, verbose=1, factor=0.1)
terminate_on_nan = TerminateOnNaN()

# add callbacks
callbacks = [model_checkpoint,
             csv_logger,
             learning_rate_scheduler,
             early_stopper,
             reduce_lr,
             terminate_on_nan]

# training parametets
n_epoch = 2
batch_size = 8
steps_per_epoch = len(train_x) // batch_size

###############
# train model #
###############
history = model.fit_generator(generator=train_generator,
                              steps_per_epoch=steps_per_epoch,
                              epochs=n_epoch,
                              callbacks=callbacks,
                              validation_data=val_generator,
                              validation_steps=steps_per_epoch)

########################
# evaluate on test set #
########################
test_x_path = os.path.join(data_dir + os.sep, 'camelyonpatch_level_2_split_test_x.h5')
test_y_path = os.path.join(data_dir + os.sep, 'camelyonpatch_level_2_split_test_y.h5')

test_dataset = DataGenerator(hdf5_images_path=test_x_path,
                             hdf5_images_dataset='x',
                             hdf5_labels_path=test_y_path,
                             hdf5_labels_dataset='y',
                             n_classes=n_classes)

test_generator = test_dataset.generate(batch_size=32,
                                       shuffle=False,
                                       transformations=[standardize],
                                       to_one_hot=True,
                                       returns={'processed_images', 'labels'})

test_x = HDF5Matrix(test_x_path, 'x')
test_y = HDF5Matrix(test_y_path, 'y')

evaluate = model.evaluate_generator(generator=test_generator,
                                    steps=steps_per_epoch)
print("Loss: ", evaluate[0], "Accuracy: ", evaluate[1])
